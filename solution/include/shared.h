#ifndef ASSIGNMENT_3_IMAGE_ROTATION_SHARED_H
#define ASSIGNMENT_3_IMAGE_ROTATION_SHARED_H

#include <stdint.h>
struct pixel {
  uint8_t b, g, r;
};

struct image {
  uint64_t width, height;
  struct pixel *data;
};

#endif // ASSIGNMENT_3_IMAGE_ROTATION_SHARED_H
