#ifndef ASSIGNMENT_3_IMAGE_ROTATION_ROTATOR_H
#define ASSIGNMENT_3_IMAGE_ROTATION_ROTATOR_H
#include <shared.h>

void rotate(struct image *source);

#endif // ASSIGNMENT_3_IMAGE_ROTATION_ROTATOR_H
