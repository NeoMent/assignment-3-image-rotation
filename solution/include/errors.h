#ifndef ASSIGNMENT_3_IMAGE_ROTATION_ERRORS_H
#define ASSIGNMENT_3_IMAGE_ROTATION_ERRORS_H

#define WRONG_FILETYPE 1
#define WRONG_HEADER 2
#define NOT_ENOUGH_MEMORY 3
#define WRONG_ENCODING 4

#define WRITE_ERROR 1

void print_read_error(int error_code);

void print_write_error(int error_code);

#endif // ASSIGNMENT_3_IMAGE_ROTATION_ERRORS_H
