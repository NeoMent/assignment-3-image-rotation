#include "errors.h"
#include <stdio.h>

void print_read_error(int error_code) {
  switch (error_code) {
  case WRONG_FILETYPE:
    perror("Неверный тип файла.\n");
    break;
  case WRONG_HEADER:
    perror("Неверный header BMP файла.\n");
    break;
  case NOT_ENOUGH_MEMORY:
    perror("Недостаточно памяти для загрузки изображения.\n");
    break;
  case WRONG_ENCODING:
    perror("Неверная кодировка цветов BMP файла.\n");
    break;
  default:
    break;
  }
}

void print_write_error(int error_code) {
  switch (error_code) {
  case WRITE_ERROR:
    perror("Ошибка при записи файла.\n");
    break;
  default:
    break;
  }
}
