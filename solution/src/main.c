#include <bmp_io.h>
#include <rotator.h>
#include <shared.h>

#include "errors.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  if (argc < 4) {
    printf("Недостаточно аргументов.");
    return 0;
  }

  if (argc > 4) {
    printf("Слишком много аргументов.");
    return 0;
  }

  //./image-transformer <source-image> <transformed-image> <angle>

  char *source_path = argv[1];
  char *target_path = argv[2];
  long angle = strtol(argv[3], NULL, 10);
  if (errno == EINVAL || errno == ERANGE) {
    printf("Неверное значение угла.");
    return 0;
  }

  if (angle < -270 || angle > 270 || angle % 90 != 0) {
    printf(
        "Неверный угол. Возможные значения: 0, 90, -90, 180, -180, 270, -270");
    return 0;
  }

  long spin_count = labs(angle) / 90;
  if (angle < 0)
    spin_count *= 3;

  FILE *source_file = fopen(source_path, "rb");
  struct image source_image = {0};
  int16_t read_code = from_bmp(source_file, &source_image);
  fclose(source_file);
  print_read_error(read_code);
  if (read_code) return read_code;

  if (spin_count != 0)
    while (spin_count--)
      rotate(&source_image);

  FILE *target_file = fopen(target_path, "wb");
  int16_t save_code = to_bmp(target_file, &source_image);
  fclose(target_file);
  print_write_error(save_code);
  if (save_code) return save_code;

  free(source_image.data);
  return 0;
}
