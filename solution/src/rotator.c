#include <printf.h>
#include <rotator.h>
#include <shared.h>
#include <stdlib.h>

void rotate(struct image *source) {
  uint32_t new_height = source->width;
  uint32_t new_width = source->height;
  struct pixel *new_pixels =
      malloc(new_height * new_width * sizeof(struct pixel));

  uint32_t pos = 0;
  for (uint32_t x = 1; x <= source->width; x++) {
    for (uint32_t y = 1; y <= source->height; y++, pos++) {
      new_pixels[pos] = source->data[y * source->width - x];
    }
  }

  source->width = new_width;
  source->height = new_height;
  free(source->data);
  source->data = new_pixels;
}
