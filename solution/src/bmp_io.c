#include <bmp_io.h>
#include <shared.h>
#include <stdio.h>
#include <stdlib.h>

const uint16_t BMP_CODE = 0x4D42;
const uint32_t DEFAULT_BOFFBITS = 54;
const uint32_t DEFAULT_BISIZE = 40;
const uint16_t DEFAULT_BIPLANES = 1;
const uint32_t DEFAULT_PPM = 2834;

static uint32_t calc_padding(uint32_t width) {
  uint32_t bytes = sizeof(struct pixel) * width;
  return (4 - bytes % 4) % 4;
}

enum read_status from_bmp(FILE *in, struct image *img) {
  struct bmp_header *header =
      (struct bmp_header *)malloc(sizeof(struct bmp_header));

  if (fread(header, sizeof(struct bmp_header), 1, in) == 0) {
    free(header);
    return READ_INVALID_HEADER;
  }

  if (header->bfType != BMP_CODE) {
    free(header);
    return READ_INVALID_TYPE;
  }

  if (header->biBitCount != sizeof(struct pixel) * 8) {
    free(header);
    return READ_UNSUPPORTED_BITS;
  }

  fseek(in, header->bOffBits, SEEK_SET);
  struct pixel *pixels = (struct pixel *)malloc(
      header->biHeight * header->biWidth * sizeof(struct pixel));
  if (pixels == NULL) {
    free(header);
    return READ_OUT_OF_MEMORY;
  }

  uint32_t padding = calc_padding(header->biWidth);
  for (uint32_t i = 0; i < header->biHeight; i++) {
    fread(pixels + i * header->biWidth, header->biWidth * sizeof(struct pixel),
          1, in);
    fseek(in, padding, SEEK_CUR);
  }

  img->data = pixels;
  img->height = header->biHeight;
  img->width = header->biWidth;

  free(header);
  return READ_OK;
}

static struct bmp_header construct_header(struct image const *img) {
  struct bmp_header header;
  header.bfType = BMP_CODE;
  header.bfileSize = sizeof(struct bmp_header) +
                     img->height * img->width * sizeof(struct pixel) * 8 +
                     calc_padding(img->width) * img->height;
  header.bfReserved = 0;
  header.bOffBits = DEFAULT_BOFFBITS;
  header.biSize = DEFAULT_BISIZE;
  header.biWidth = img->width;
  header.biHeight = img->height;
  header.biPlanes = DEFAULT_BIPLANES;
  header.biBitCount = sizeof(struct pixel) * 8;
  header.biCompression = 0;
  header.biSizeImage = 0;
  header.biXPelsPerMeter = DEFAULT_PPM;
  header.biYPelsPerMeter = DEFAULT_PPM;
  header.biClrUsed = 0;
  header.biClrImportant = 0;

  return header;
}

enum write_status to_bmp(FILE *out, struct image *img) {
  struct bmp_header header = construct_header(img);
  uint32_t padding = calc_padding(header.biWidth);
  if (fwrite(&header, sizeof(struct bmp_header), 1, out) < 1)
    return WRITE_ERROR;

  for (uint32_t i = 0; i < header.biHeight; i++) {
    fwrite(img->data + i * img->width, img->width * sizeof(struct pixel), 1,
           out);
    fseek(out, padding, SEEK_CUR);
  }

  return WRITE_OK;
}
